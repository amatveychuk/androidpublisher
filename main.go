package main

import (
	"context"
	"flag"
	"fmt"
	"google.golang.org/api/androidpublisher/v3"
	"google.golang.org/api/googleapi"
	"os"
)

func main() {
	packageNameArg := flag.String("package", "", "App package name")
	bundleFileNameArg := flag.String("bundle", "", "Bundle file path")
	trackArg := flag.String("track", "internal", "Track to release to")
	flag.Parse()

	packageName := *packageNameArg
	bundleFilePath := *bundleFileNameArg
	trackName := *trackArg

	ctx := context.Background()
	service, err := androidpublisher.NewService(ctx)
	logErrorAndExit(err, "Error while creating service")
	appEdit, err := service.Edits.Insert(packageName, &androidpublisher.AppEdit{}).Do()
	logErrorAndExit(err, "Error while creating edit")
	uploadReq := service.Edits.Bundles.Upload(packageName, appEdit.Id)
	fileReader, err := os.Open(bundleFilePath)
	logErrorAndExit(err, "Cannot open bundle file")
	uploadReq.Media(
		fileReader,
		googleapi.ContentType("application/octet-stream"),
	)
	bundleInfo, err := uploadReq.Do()
	logErrorAndExit(err, "Error while uploading bundle")
	trackInfo, err := service.Edits.Tracks.Get(packageName, appEdit.Id, trackName).Do()
	logErrorAndExit(err, "Error while getting track info")
	trackInfo.Releases = []*androidpublisher.TrackRelease{
		{
			Status:       "completed",
			VersionCodes: []int64{bundleInfo.VersionCode},
		},
	}
	_, err = service.Edits.Tracks.Patch(packageName, appEdit.Id, trackName, trackInfo).Do()
	logErrorAndExit(err, "Error while patching track")
	_, err = service.Edits.Commit(packageName, appEdit.Id).Do()
	logErrorAndExit(err, "Error while committing edit")
	fmt.Printf("[Ok] Release %d published to %s track.\n", bundleInfo.VersionCode, trackName)
}

func logErrorAndExit(err error, message string) {
	if err != nil {
		fmt.Printf("[Error] %s\n", message)
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
